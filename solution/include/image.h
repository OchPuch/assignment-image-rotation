#ifndef ASSIGNMENT_IMAGE_ROTATIONFORK_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATIONFORK_IMAGE_H

#include <stdint.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(uint64_t width, uint64_t height); // allocates memory for image data

void destroy_image(struct image img); // frees memory allocated for image data

#endif //ASSIGNMENT_IMAGE_ROTATIONFORK_IMAGE_H
