#ifndef ASSIGNMENT_IMAGE_ROTATIONFORK_TRANSFORMER_H
#define ASSIGNMENT_IMAGE_ROTATIONFORK_TRANSFORMER_H
#include "image.h"

enum rotation_direction {
    CLOCKWISE,
    COUNTERCLOCKWISE
};

struct image rotateLeft( struct image const source );
struct image rotateRight( struct image const source );
struct image multipleRotations( struct image const source, uint8_t count, enum rotation_direction direction );

#endif //ASSIGNMENT_IMAGE_ROTATIONFORK_TRANSFORMER_H
