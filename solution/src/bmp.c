#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

uint8_t get_padding(uint64_t width) {
    return (uint8_t)((4 - (width * sizeof(struct pixel)) % 4) % 4);
}

enum read_status checkHeader(struct bmp_header header) {
    if (header.bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    return READ_OK;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    enum read_status status = READ_OK;
    if (!fread(&header, sizeof(header), 1, in)) {
        return READ_ERROR;
    }

    status = checkHeader(header);
    if (status != READ_OK) {
        return status;
    }


    if (fseek(in, header.bOffBits, SEEK_SET)) {
        return READ_ERROR;
    }
    struct image temp = create_image(header.biWidth, header.biHeight);

    const uint8_t PADDING = get_padding(temp.width);

    for (size_t i = 0; i < temp.height; i += 1) {
        for (size_t j = 0; j < temp.width; j += 1) {
            if (!fread(&temp.data[i * temp.width + j], sizeof(struct pixel), 1, in)) {
                status = READ_ERROR;
                break;
            }
        }
        // padding
        if (PADDING) {
            if (fseek(in, (long int) PADDING, SEEK_CUR)) {
                status = READ_ERROR;
                break;
            }
        }
    }

    if (status == READ_OK) {
        *img = temp;
    } else {
        destroy_image(temp);
    }

    return READ_OK;
}


struct bmp_header generate_header(uint64_t width, uint64_t height) {
    struct bmp_header header;
    header.bfType = 0x4D42;
    header.bfileSize = sizeof(header) + width * height * sizeof(struct pixel);
    header.bfReserved = 0;
    header.bOffBits = sizeof(header);
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = width * height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = generate_header(img->width, img->height);
    if (!fwrite(&header, sizeof(header), 1, out)) {
        return WRITE_ERROR;
    }
    const uint8_t PADDING = get_padding(img->width);
    const char *ZERO_STRING = "000";
    fseek(out, header.bOffBits, SEEK_SET);
    for (size_t i = 0; i < img->height; i += 1) {
        for (size_t j = 0; j < img->width; j += 1) {
            if (!fwrite(&img->data[i * img->width + j], sizeof(struct pixel), 1, out)) {
                return WRITE_ERROR;
            }
        }
        //PADDING
        if (PADDING) {
            if (!fwrite(ZERO_STRING, (long int) PADDING, 1, out)) {
                return WRITE_ERROR;
            }
        }

    }
    return WRITE_OK;
}
