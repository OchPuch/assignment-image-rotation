#include "image.h"
#include <stdlib.h>

struct image create_image(uint64_t width, uint64_t height){
    struct image img;
    img.width = width;
    img.height = height;
    img.data = malloc(width * height * sizeof(struct pixel));
    return img;
}

void destroy_image(struct image img){
    if (img.data != NULL){
        free(img.data);
    }
}
