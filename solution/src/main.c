#include "image.h"
#include "bmp.h"
#include "transformer.h"
#include <stdio.h>
#include <stdlib.h>

int read_from_file(const char* input_filename, struct image* source)
{
    FILE *input_file = fopen(input_filename, "rb");
    if (input_file == NULL) {
        fprintf(stderr, "Could not open file %s", input_filename);
        return 1;
    }

    int temp_result = (int) from_bmp(input_file, source);
    fclose(input_file);
    return temp_result;
}

int write_to_file(const char* output_filename, struct image* result)
{
    FILE *output_file = fopen(output_filename, "wb");
    if (output_file == NULL) {
        fprintf(stderr, "Could not open file %s", output_filename);
        return 1;
    }

    int temp_result = (int) to_bmp(output_file, result);
    fclose(output_file);
    return temp_result;
}

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <input.bmp> <output.bmp>", argv[0]);
        return 1;
    }

    char *input_filename = argv[1];
    char *output_filename = argv[2];

    struct image source;

    if (read_from_file(input_filename,&source)){
        return 1;
    }

    struct image result = multipleRotations(source, 1, COUNTERCLOCKWISE);
    destroy_image(source);

    if (write_to_file(output_filename, &result))
    {
        destroy_image(result);
        return 1;
    }

    destroy_image(result);
    return 0;
}
