#include "image.h"
#include "transformer.h"
#include <stdlib.h>

struct image rotateLeft(struct image const source) {
    struct image result;

    result.width = source.height;
    result.height = source.width;
    size_t isZero = !result.width || !result.height;
    if (isZero) {
        result.data = NULL;
        return result;
    }

    result.data = malloc(result.width * result.height * sizeof(struct pixel));

    for (size_t i = 0; i < source.height; i+=1) {
        for (size_t j = 0; j < source.width; j+=1) {
            result.data[j * result.width + (result.width - i - 1)] = source.data[i * source.width + j];
        }
    }

    return result;
}

struct image rotateRight(struct image const source) {
    struct image result;

    result.width = source.height;
    result.height = source.width;
    //check for error: Call to 'malloc' has an allocation size of 0 bytes
    int isZero = !result.width || !result.height;
    if (isZero) {
        result.data = NULL;
        return result;
    }
    result.data = malloc(result.width * result.height * sizeof(struct pixel));

    for (size_t i = 0; i < source.height; i+=1) {
        for (size_t j = 0; j < source.width; j+=1) {
            result.data[(result.height - j - 1) * result.width + i] = source.data[i * source.width + j];
        }
    }

    return result;
}

struct image multipleRotations(struct image const source, uint8_t count, enum rotation_direction direction) {
    struct image result;

    if (direction == CLOCKWISE) {
        result = rotateRight(source);
        //check for error: Potential memory leak - 'result' may not be freed or pointed-to in the function 'multipleRotations'
        for (size_t i = 1; i < count; i+=1) {
            struct image temp = rotateRight(result);
            free(result.data);
            result = temp;
        }

    } else {
        result = rotateLeft(source);
        for (size_t i = 1; i < count; i+=1) {
            struct image temp = rotateRight(result);
            free(result.data);
            result = temp;
        }
    }

    return result;
}


